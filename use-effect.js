const App = () => {
  
  const [windowWidthSize, setWindowWidthSize] = React.useState(0);

  React.useEffect(() => {
    const handleResize = (e) => {
      const { width } = document.body.getBoundingClientRect();
      setWindowWidthSize(Math.ceil(width));
    }

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return (
    <h1>
      The window size {windowWidthSize} pixels
    </h1>
  )
};

render(<App />, document.querySelector("#root"));