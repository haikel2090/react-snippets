function App() {
  const [text] = React.useState('Hello world');

  return <div>I am learning {text}</div>;
}

render(<App />, document.getElementById('root'))