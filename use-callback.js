function Button(props) {
  return <button onClick={props.onClick}>{props.name}</button>;
}

function App() {
  const [count, setCount] = React.useState(0);
  const [isActive, setActive] = React.useState(false);

  const handleCount = React.useCallback(() => setCount(count + 1), [count]);
  const handleShow = React.useCallback(() => setActive(!isActive), [isActive]);

  return (
    <div className="App">
      {isActive && (
        <div>
          <h1>{count}</h1>
          <Button onClick={handleCount} name="Increment" />
        </div>
      )}
      <Button
        onClick={handleShow}
        name={isActive ? "Hide Counter" : "Show Counter"}
      />
    </div>
  );
}

render(<App />, document.getElementById("root"));