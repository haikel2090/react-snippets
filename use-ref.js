function App() {

  const [query, setQuery] = React.useState("react hooks");
  const searchInput = React.useRef(null);

  const clear = () => {
    searchInput.current.value = "";
    searchInput.current.focus();
  }

  return (
    <form>
      <input
        type="text"
        onChange={event => setQuery(event.target.value)}
        ref={searchInput}
      />
      <button type="submit">Search</button>
      <button type="button" onClick={clear}>
        Clear
      </button>
    </form>
  );
}

render(<App />, document.getElementById('root'))